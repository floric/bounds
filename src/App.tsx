import { Component, lazy } from "solid-js";
import { RouteDefinition, Router, useRoutes } from "solid-app-router";

const routes: Array<RouteDefinition> = [
  {
    path: "/",
    component: lazy(() => import("./views/Root")),
  },
  {
    path: "/:lang/",
    component: lazy(() => import("./views/Home")),
  },
  {
    path: "*",
    component: lazy(() => import("./views/NotFound")),
  },
];

const App: Component = () => {
  const Routes = useRoutes(routes);

  return (
    <div class="text-dark">
      <Router>
        <Routes />
      </Router>
    </div>
  );
};

export default App;
