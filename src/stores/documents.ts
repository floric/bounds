import Fuse from "fuse.js";
import { createMemo } from "solid-js";
import { createStore } from "solid-js/store";
import { allPolicies } from "../data/policies";
import { Role } from "../data/roles";
import { Tag } from "../data/tags";
import { Policy, PolicyDocument } from "../types";

type DocumentsStore = {
  results: Array<Policy>;
  filters: {
    searchQuery: string;
    tagIds: Array<string>;
    docIds: Array<string>;
    roleIds: Array<string>;
  };
};

const fuse = new Fuse(allPolicies, {
  keys: [
    { name: "summary", weight: 3 },
    "explanation",
    "actions.action",
    "actions.condition",
  ],
  threshold: 0.5,
});

const [state, setState] = createStore<DocumentsStore>({
  results: [],
  filters: {
    searchQuery: "",
    tagIds: [],
    docIds: [],
    roleIds: [],
  },
});

export const setSearchQuery = (query: string) => {
  setState((state) => ({
    ...state,
    filters: {
      ...state.filters,
      searchQuery: query || "",
    },
  }));
};

export const addTagFilter = (tag: Tag) =>
  setState((state) => ({
    ...state,
    filters: {
      ...state.filters,
      tagIds: [...state.filters.tagIds, tag.id],
    },
  }));

export const removeTagFilter = (tag: Tag) =>
  setState((state) => ({
    ...state,
    filters: {
      ...state.filters,
      tagIds: [...state.filters.tagIds.filter((t) => t !== tag.id)],
    },
  }));

export const addRoleFilter = (role: Role) =>
  setState((state) => ({
    ...state,
    filters: {
      ...state.filters,
      roleIds: [...state.filters.roleIds, role.id],
    },
  }));

export const removeRoleFilter = (role: Role) =>
  setState((state) => ({
    ...state,
    filters: {
      ...state.filters,
      roleIds: [...state.filters.roleIds.filter((t) => t !== role.id)],
    },
  }));

export const addDocFilter = (doc: PolicyDocument) =>
  setState((state) => ({
    ...state,
    filters: {
      ...state.filters,
      docIds: [...state.filters.docIds, doc.id],
    },
  }));

export const removeDocFilter = (doc: PolicyDocument) =>
  setState((state) => ({
    ...state,
    filters: {
      ...state.filters,
      docIds: [...state.filters.docIds.filter((t) => t !== doc.id)],
    },
  }));

export const resetFilters = () => {
  setState((state) => ({
    ...state,
    filters: { searchQuery: "", docIds: [], roleIds: [], tagIds: [] },
  }));
};

export const filters = createMemo(() => state.filters);

export const hasActiveFilters = createMemo(
  () =>
    state.filters.tagIds.length > 0 ||
    state.filters.docIds.length > 0 ||
    state.filters.roleIds.length > 0
);

export const hasTagFilter = (tag: Tag) =>
  filters().tagIds.find((t) => t === tag.id) !== undefined;

export const hasDocFilter = (doc: PolicyDocument) =>
  filters().docIds.find((t) => t === doc.id) !== undefined;

export const hasRoleFilter = (role: Role) =>
  filters().roleIds.find((t) => t === role.id) !== undefined;

const filterByDocId = (policy: Policy) =>
  state.filters.docIds
    .map((neededDocId) => neededDocId === policy.document.id)
    .reduce((a, b) => a && b, true);

const filterByRole = (policy: Policy): boolean =>
  state.filters.roleIds
    .map(
      (neededRoleId) =>
        policy.roles.find((role) => role.id === neededRoleId) !== undefined
    )
    .reduce((a, b) => a && b, true);

const filterByTag = (policy: Policy): boolean =>
  state.filters.tagIds
    .map(
      (neededTagId) =>
        policy.tags.find((tag) => tag.id === neededTagId) !== undefined
    )
    .reduce((a, b) => a && b, true);

export const filteredDocuments = createMemo(() => {
  const policiesToFilter =
    state.filters.searchQuery.length > 0
      ? fuse.search(state.filters.searchQuery).map((r) => r.item)
      : allPolicies;

  const documents = policiesToFilter
    .filter(filterByTag)
    .filter(filterByRole)
    .filter(filterByDocId)
    .map((policy) => ({
      [policy.document.id]: { document: policy.document, policies: [policy] },
    }))
    .reduce((res, obj) => {
      const docId = Object.keys(obj)[0];
      if (res[docId] === undefined) {
        return { ...res, ...obj };
      }
      return {
        ...res,
        [docId]: {
          document: obj[docId].document,
          policies: [...res[docId].policies, obj[docId].policies[0]],
        },
      };
    }, {});

  return Object.values(documents);
});

export const matchingTags = createMemo(() => {
  const allTags = filteredDocuments()
    .flatMap((doc) => doc.policies)
    .flatMap((p) => p.tags)
    .map((tag) => ({ [tag.id]: tag }))
    .reduce((a, b) => ({ ...a, ...b }), {});
  return Object.values(allTags).sort((a, b) => a.label.localeCompare(b.label));
});

export const matchingRoles = createMemo(() => {
  const allRoles = filteredDocuments()
    .flatMap((doc) => doc.policies)
    .flatMap((p) => p.roles)
    .map((role) => ({ [role.id]: role }))
    .reduce((a, b) => ({ ...a, ...b }), {});
  return Object.values(allRoles).sort((a, b) => a.label.localeCompare(b.label));
});

export const matchingDocuments = createMemo(() => {
  const allDocs = filteredDocuments()
    .flatMap((doc) => doc.policies)
    .flatMap((p) => p.document)
    .map((doc) => ({ [doc.id]: doc }))
    .reduce((a, b) => ({ ...a, ...b }), {});
  return Object.values(allDocs).sort((a, b) => a.name.localeCompare(b.name));
});
