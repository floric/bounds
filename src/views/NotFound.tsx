import { Component } from "solid-js";
import Header from "../components/typography/Header";

const NotFound: Component = () => {
  return (
    <div>
      <Header level={1}>Not Found</Header>
      <p>This page seems to be missing. :(</p>
    </div>
  );
};

export default NotFound;
