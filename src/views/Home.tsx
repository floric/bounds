import { Component } from "solid-js";
import Footer from "../components/layout/Footer";
import Filters from "./components/Filters";
import Results from "./components/Results";

const Home: Component = () => {
  return (
    <div class="flex flex-col min-h-screen justify-between bg-bg-light">
      <main class="mt-2 mb-8">
        <div class="max-w-7xl mx-auto py-6 px-2 lg:px-8 grid grid-cols-search gap-8">
          <Filters />
          <Results />
        </div>
      </main>
      <Footer />
    </div>
  );
};

export default Home;
