import { Component } from "solid-js";
import { Navigate } from "solid-app-router";

const Root: Component = () => {
  return <Navigate href="/en/" />;
};

export default Root;
