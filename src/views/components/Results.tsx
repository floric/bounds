import { Component, For, Show } from "solid-js";
import Paragraph from "../../components/typography/Paragraph";
import { filteredDocuments, filters } from "../../stores/documents";
import DocumentEntry from "./DocumentEntry";

const Results: Component<{}> = () => {
  return (
    <div class="flex flex-col gap-4">
      <For each={filteredDocuments()}>
        {(doc) => (
          <DocumentEntry document={doc.document} policies={doc.policies} />
        )}
      </For>
      <Show when={filteredDocuments().length === 0}>
        <Paragraph>No results found for "{filters().searchQuery}".</Paragraph>
      </Show>
    </div>
  );
};

export default Results;
