import { formatDistanceToNowStrict } from "date-fns";
import { Icon } from "solid-heroicons";
import {
  clock,
  exclamation,
  questionMarkCircle,
} from "solid-heroicons/outline";
import { Component, For, Match, Show, Switch } from "solid-js";
import { Frequency, Periodic, Policy } from "../../types";

interface Props {
  policy: Policy;
}

const tryMatchFrequencyPeriodic = (element: Frequency): Periodic | false =>
  element.kind === "periodic" ? element : false;

const PolicyEntry: Component<Props> = (props) => {
  return (
    <div class="flex flex-col mb-2">
      <div class="flex justify-between">
        <strong>{props.policy.summary}</strong>
        <div class="text-gray-500">
          <Icon path={clock} class="inline h-4" />{" "}
          {formatDistanceToNowStrict(props.policy.validityDate, {
            addSuffix: true,
          })}
        </div>
      </div>
      <div class="-1">
        <div class="max-w-4xl text-justify">
          <Icon path={questionMarkCircle} class="inline h-4" />{" "}
          {props.policy.explanation}
        </div>
        <For each={props.policy.actions}>
          {(action) => (
            <div>
              <Icon path={exclamation} class="inline h-4" /> {action.action}
              <Show when={action.frequency.kind !== "always"}>
                <>
                  {" "}
                  (
                  <Switch>
                    <Match when={tryMatchFrequencyPeriodic(action.frequency)}>
                      {(matched) => `every ${matched.value} ${matched.period}`}
                    </Match>
                  </Switch>
                  )
                </>
              </Show>
            </div>
          )}
        </For>
      </div>
    </div>
  );
};

export default PolicyEntry;
