import { Icon } from "solid-heroicons";
import { externalLink, document } from "solid-heroicons/outline";
import { Component, For } from "solid-js";
import Header from "../../components/typography/Header";
import Link from "../../components/typography/Link";
import { Policy, PolicyDocument } from "../../types";
import PolicyEntry from "./PolicyEntry";

type Props = {
  document: PolicyDocument;
  policies: Array<Policy>;
};

const DocumentEntry: Component<Props> = (props) => {
  return (
    <div>
      <Header level={3}>
        {props.document.name} |{" "}
        <Link href={props.document.referenceUrl} target="_href">
          <span class="font-sans">Source</span>{" "}
          <Icon path={externalLink} class="inline h-4" />
        </Link>
      </Header>
      <div class="ml-2 mt-1">
        <For each={props.policies}>
          {(policy) => <PolicyEntry policy={policy} />}
        </For>
      </div>
    </div>
  );
};

export default DocumentEntry;
