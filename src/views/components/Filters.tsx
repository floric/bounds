import { useSearchParams } from "solid-app-router";
import { Icon } from "solid-heroicons";
import { documentDuplicate } from "solid-heroicons/outline";
import { Component, createEffect, For } from "solid-js";
import Button from "../../components/inputs/Button";
import Checkbox from "../../components/inputs/Checkbox";
import SearchInput from "../../components/inputs/SearchInput";
import Header from "../../components/typography/Header";
import {
  setSearchQuery,
  matchingTags,
  hasTagFilter,
  addTagFilter,
  removeTagFilter,
  matchingDocuments,
  hasDocFilter,
  addDocFilter,
  removeDocFilter,
  matchingRoles,
  hasRoleFilter,
  addRoleFilter,
  removeRoleFilter,
  hasActiveFilters,
  resetFilters,
  filteredDocuments,
} from "../../stores/documents";

const Filters: Component<{}> = (props) => {
  const [searchParams, setSearchParams] = useSearchParams<{ q: string }>();
  createEffect(() => {
    setSearchQuery(searchParams.q);
  });
  return (
    <div class="flex flex-col gap-4">
      <SearchInput
        size="small"
        onSearch={(query) => {
          setSearchQuery(query);
          setSearchParams({ q: query });
        }}
        query={searchParams.q}
      />
      <div>
        <Header level={5}>Tags</Header>
        <For each={matchingTags()}>
          {(tag) => (
            <Checkbox
              label={tag.label}
              value={tag.id}
              checked={hasTagFilter(tag)}
              onSelect={(newState) =>
                newState ? addTagFilter(tag) : removeTagFilter(tag)
              }
            />
          )}
        </For>
      </div>
      <div>
        <Header level={5}>Documents</Header>
        <For each={matchingDocuments()}>
          {(doc) => (
            <Checkbox
              label={doc.name}
              value={doc.id}
              checked={hasDocFilter(doc)}
              onSelect={(newState) =>
                newState ? addDocFilter(doc) : removeDocFilter(doc)
              }
            />
          )}
        </For>
      </div>
      <div>
        <Header level={5}>Roles</Header>
        <For each={matchingRoles()}>
          {(role) => (
            <Checkbox
              label={role.label}
              value={role.id}
              checked={hasRoleFilter(role)}
              onSelect={(newState) =>
                newState ? addRoleFilter(role) : removeRoleFilter(role)
              }
            />
          )}
        </For>
      </div>
      <div class="flex justify-between items-baseline">
        <Button disabled={!hasActiveFilters()} onClick={resetFilters}>
          Reset Filters
        </Button>
      </div>
      <div>
        <Icon path={documentDuplicate} class="inline h-4" />{" "}
        {filteredDocuments().length} Documents,{" "}
        {filteredDocuments()
          .map((n) => n.policies.length)
          .reduce((a, b) => a + b, 0)}{" "}
        Policies
      </div>
    </div>
  );
};

export default Filters;
