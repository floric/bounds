import { Icon } from "solid-heroicons";
import { arrowCircleUp } from "solid-heroicons/outline";
import { Component } from "solid-js";
import Button from "../inputs/Button";
import Link from "../typography/Link";

const Footer: Component = () => {
  return (
    <footer class="fixed bottom-0 w-full flex flex-col bg-bg-dark p-1 text-light text-xs">
      <div class="sm:grid sm:grid-cols-3 gap-4 flex flex-col items-center">
        <div class="inline-flex gap-3 my-1">
          <div>
            <Link href="#">About</Link>
          </div>
          <div>
            <Link href="#">Contact</Link>
          </div>
          <div>
            <Link href="#">Privacy</Link>
          </div>
        </div>
        <div class="text-center my-1">Made in Erfurt</div>
        <div class="text-right">
          <Button kind="transparent" onClick={() => scrollTo({ top: 0 })}>
            <Icon path={arrowCircleUp} class="inline h-4" /> Top
          </Button>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
