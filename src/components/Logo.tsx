import { Component, mergeProps } from "solid-js";

interface Props {
  isDark?: boolean;
  size?: "small" | "regular";
}

const Logo: Component<Props> = (props) => {
  const merged = mergeProps({ isDark: false, size: "regular" }, props);
  return (
    <div class="flex gap-2 items-center">
      <div class="flex flex-col gap-1 items-end justify-center w-4">
        <div class="bg-secondary-2 w-2 h-1"></div>
        <div class="bg-secondary-1 w-3 h-1"></div>
        <div class="bg-primary w-4 h-1"></div>
      </div>
      <div
        class={`${merged.size === "regular" ? "text-4xl" : "text-xl"} ${
          merged.isDark ? "text-light" : "text-dark"
        } font-serif`}
      >
        bounds.
      </div>
    </div>
  );
};

export default Logo;
