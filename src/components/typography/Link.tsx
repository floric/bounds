import { Component } from "solid-js";
import { Link, LinkProps } from "solid-app-router";

const LinkComponent: Component<LinkProps> = (props) => {
  return (
    <Link href={props.href} target={props.target} class="text-complementary">
      {props.children}
    </Link>
  );
};

export default LinkComponent;
