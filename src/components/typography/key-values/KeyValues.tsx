import { Component } from "solid-js";

const KeyValues: Component = (props) => {
  return <div class="grid grid-cols-6 gap-2">{props.children}</div>;
};

export default KeyValues;
