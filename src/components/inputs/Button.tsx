import { Component, JSX } from "solid-js";

type Props = {
  kind?: "primary" | "secondary" | "transparent";
  size?: "small" | "regular";
} & JSX.ButtonHTMLAttributes<HTMLButtonElement>;

const Button: Component<Props> = (props) => {
  const kind = props.kind || "secondary";
  const size = props.size || "small";
  return (
    <button
      {...props}
      classList={{
        "rounded box-border leading-none transition outline-none focus:outline-none focus:ring focus:ring-secondary-1":
          true,
        "px-2 py-1": size === "small",
        "px-3 py-2": size === "regular",
        "bg-primary": kind === "primary",
        "bg-transparent border border-black":
          kind === "secondary" && !props.disabled,
        "hover:bg-gray-200":
          (kind === "primary" || kind === "secondary") && !props.disabled,
        "bg-transparent hover:bg-gray-700": kind === "transparent",
        "hover:bg-gray-700": kind === "transparent" && !props.disabled,
        "text-gray-300 border border-gray-300": props.disabled,
      }}
    >
      {props.children}
    </button>
  );
};

export default Button;
