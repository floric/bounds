import { Component } from "solid-js";

type Props = {
  checked: boolean;
  label: string;
  value: string;
  onSelect: (newState: boolean) => void;
};

const Checkbox: Component<Props> = (props) => {
  return (
    <div>
      <input
        id={`check-${props.value}`}
        type="checkbox"
        value={props.value}
        class="mr-2"
        onChange={(ev) => {
          props.onSelect((ev.target as any).checked);
        }}
        checked={props.checked}
      />
      <label for={`check-${props.value}`}>{props.label}</label>
    </div>
  );
};

export default Checkbox;
