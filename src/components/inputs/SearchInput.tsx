import { Component, mergeProps } from "solid-js";
import { Icon } from "solid-heroicons";
import { search } from "solid-heroicons/outline";

interface Props {
  query?: string;
  size?: "small" | "regular";
  onSearch: (query: string) => void;
}

const SearchInput: Component<Props> = (props) => {
  const merged = mergeProps({ size: "regular" }, props);

  const debounceInput = (func: (query: string) => unknown, delay = 300) => {
    let timeout: number;

    return (e: { currentTarget: HTMLInputElement }) => {
      const query = e.currentTarget.value;
      clearTimeout(timeout);
      timeout = setTimeout(() => func(query), delay);
    };
  };

  return (
    <div class="relative">
      <div class="flex flex-wrap items-stretch">
        <span
          class="z-10 h-full leading-normal font-normal absolute text-center text-gray-400 rounded items-center justify-center w-8"
          classList={{
            "pl-3 py-3": merged.size === "regular",
            "pl-1 py-1": merged.size !== "regular",
          }}
        >
          <Icon path={search} class="h-5 inline p-0 m-0" />
        </span>
        <input
          autofocus
          autocomplete="off"
          type="text"
          placeholder="Search"
          value={props.query || ""}
          classList={{
            "placeholder-gray-400 text-dark bg-white rounded text-base border border-gray-400 outline-none transition focus:outline-none focus:ring focus:ring-secondary-1 w-full":
              true,
            "p-3 pl-10": merged.size === "regular",
            "p-1 pl-8": merged.size !== "regular",
          }}
          oninput={debounceInput(props.onSearch)}
        />
      </div>
    </div>
  );
};

export default SearchInput;
