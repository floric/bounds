import { Component } from "solid-js";

interface Props {
  message: string;
}

const SearchMessage: Component<Props> = (props) => {
  return (
    <div class="absolute shadow-lg rounded bg-white text-dark w-full px-3 py-2">
      {props.message}
    </div>
  );
};

export default SearchMessage;
