import { Link } from "solid-app-router";
import { Component } from "solid-js";

interface Props {
  name: string;
  isSelected: boolean;
  onSelect: () => void;
}

const SearchResult: Component<Props> = (props) => {
  return (
    <li>
      <Link
        onClick={props.onSelect}
        class="block m-1 px-3 py-2"
        classList={{
          "bg-primary font-black rounded": props.isSelected,
        }}
        href={`/en/topic/${props.name}`}
      >
        {props.name}
      </Link>
    </li>
  );
};

export default SearchResult;
