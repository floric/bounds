import { Role } from "./data/roles";
import { Tag } from "./data/tags";

export type Frequency = Periodic | Always;

export type Periodic = {
  kind: "periodic";
  period: "years" | "months" | "days";
  value: number;
};

export type Always = {
  kind: "always";
};

export type PolicyDocument = {
  id: string;
  name: string;
  referenceUrl: string;
};

export type Condition = {
  id: string;
  description: string;
};

export type ActionItem = {
  frequency: Frequency;
  condition: string | null;
  action: string;
};

export type Policy = {
  id: string;
  validityDate: Date;
  summary: string;
  explanation: string | null;
  actions: Array<ActionItem>;
  tags: Array<Tag>;
  document: PolicyDocument;
  roles: Array<Role>;
};
